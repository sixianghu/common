package com.smartlock.util;

import org.apache.commons.lang3.StringUtils;

public class BytesUtils {

    public static String formatBytesString(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (byte aByte : bytes) {
            stringBuilder.append((int) aByte);
            stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    public static byte[] parse(String commond) {
        String[] byteStr = StringUtils.split(commond, ",");
        byte[] bytes = new byte[byteStr.length];
        for (int i = 0; i < byteStr.length; i++) {
            bytes[i] = (byte) ScaleUtil.HexToInt(byteStr[i]);
        }
        return bytes;
    }

    public static String formatHexString(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (byte aByte : bytes) {
            String byteStr = formatHexChar(aByte);
            if (byteStr != null && !"".equals(byteStr)) {
                stringBuilder.append(formatHexChar(aByte));
                stringBuilder.append(",");
            }
        }
        return stringBuilder.toString();
    }

    public static String formatHexChar(byte b) {
        return String.format("%02X", b);
    }
}
