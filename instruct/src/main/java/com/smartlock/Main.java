package com.smartlock;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by dhl on 2017/12/20.
 */
@SpringBootApplication
@ComponentScan("com.smartlock")
public class Main {

    private static volatile Map<String, Socket> macSocketMap = new ConcurrentHashMap<>();

    public static Map<String, Socket> getMacSocketMap() {
        return macSocketMap;
    }

    public static void main(String args[]) throws IOException {
        System.out.println("the main method is running!");
        SpringApplication.run(Main.class, args);
        System.out.println("the socket monitor is running");
        int port = 88;
        ServerSocket server = new ServerSocket(port);
        while (true) {
            Socket socket = server.accept();
            socket.setSoTimeout(3 * 60 * 1000);
            new Thread(new LockTask(socket)).start();
        }

    }

}
