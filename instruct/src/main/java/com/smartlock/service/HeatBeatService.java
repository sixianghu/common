package com.smartlock.service;

import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smartlock.Main;
import com.smartlock.util.BytesUtils;

public class HeatBeatService {

    private static SimpleDateFormat sdf    = new SimpleDateFormat("yyyyMMddhhmmss");
    private static Logger           logger = LoggerFactory.getLogger("HeatBeatService");

    public static byte[] heatbeatHandle(byte[] heatBeatCommand, Socket socket, String gatewayMac) throws IOException {

        //        byte[] gatewayMacBytes = new byte[6];
        //        System.arraycopy(heatBeatCommand, 8, gatewayMacBytes, 0, 6);
        //        String gatewayMac = BytesUtils.formatHexString(gatewayMacBytes);
        System.out.println("添加socket到socketMap中Mac=" + gatewayMac);
        Main.getMacSocketMap().put(gatewayMac, socket);//绑定网关mac与锁Socket连接

        logger.info("接收到心跳请求,gatewayMac=" + gatewayMac + "，command:" + BytesUtils.formatHexString(heatBeatCommand));
        byte[] returnMsg = new byte[12];
        returnMsg[0] = 43;
        returnMsg[1] = 111;
        returnMsg[2] = 107;
        String yyyyMMddhhmmss = sdf.format(new Date());
        int j = 3;
        for (int i = 0; i < 14; i = i + 2) {
            returnMsg[j] = (byte) Integer.parseInt(yyyyMMddhhmmss.substring(i, i + 2));
            j++;
        }
        returnMsg[10] = 13;
        returnMsg[11] = 10;
        return returnMsg;
    }

}
