package com.smartlock.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.smartlock.param.UnLockPushParam;
import com.smartlock.util.BytesUtils;

public class UnLockService {

    private static final String unLockBackgroundPushUrl = "http://116.62.142.20/MicroService/unlockpush";
    private static Logger       logger                  = LoggerFactory.getLogger(UnLockService.class);

    private static void unLockPush(byte[] paramMsg) {
        UnLockPushParam unLockParam = new UnLockPushParam();
        byte[] lockMac = new byte[6];
        System.arraycopy(paramMsg, 8, lockMac, 0, 6);
        byte[] lockNo = new byte[2];
        System.arraycopy(paramMsg, 14, lockNo, 0, 2);
        byte unlockType = paramMsg[17];
        unLockParam.setLockMac(BytesUtils.formatHexString(lockMac));
        unLockParam.setLockNo(BytesUtils.formatHexString(lockNo));
        unLockParam.setUnlockType(BytesUtils.formatHexChar(unlockType));
        BackgroundService.pushToBackground(unLockBackgroundPushUrl, unLockParam);
        System.out
            .println("开锁推送，开锁入参：" + BytesUtils.formatHexString(paramMsg) + ",后台推送参数：" + JSON.toJSONString(unLockParam));
    }

    public static byte[] unlockPush(byte[] b) {

        byte[] returnMsg = new byte[11];
        //特殊码
        returnMsg[0] = 13;
        returnMsg[1] = 10;
        //帧头
        returnMsg[2] = (byte) 170;
        // 随机码
        returnMsg[3] = 00;//returnMsg[3] = (byte) new Random().nextInt(100);
        //有效长度
        returnMsg[4] = 03;
        //序列码
        returnMsg[5] = b[6];
        //服务码
        returnMsg[6] = 05;
        //校验码
        returnMsg[7] = b[18];
        //帧尾
        returnMsg[8] = (byte) 204;
        //特殊码
        returnMsg[9] = 13;
        returnMsg[10] = 10;
        unLockPush(b);
        return returnMsg;
    }

}
