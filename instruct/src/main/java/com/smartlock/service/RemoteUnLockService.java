package com.smartlock.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.smartlock.param.RemoteUnLockParam;
import com.smartlock.param.RemoteUnLockPushParam;
import com.smartlock.util.BytesUtils;
import com.smartlock.util.ScaleUtil;

/**
 * Created by dhl on 2017/12/22.
 */
public class RemoteUnLockService {

    private static Logger                       logger              = LoggerFactory
        .getLogger(RemoteUnLockService.class);
    private static final String                 remoteUnLockPushUrl = "http://116.62.142.20/MicroService/unlockstatus";

    private static volatile Map<String, String> remoteUnLockMap     = new ConcurrentHashMap<>();
    private static volatile Map<String, String> lockMacNoMap        = new ConcurrentHashMap<>();
    private static AtomicInteger                xulieCode           = new AtomicInteger();

    public static void pushLockMsgToBackground(byte[] paramMsg, String gatewayMac, String unlockResult) {
        RemoteUnLockPushParam param = new RemoteUnLockPushParam();

        byte[] lockMacBytes = new byte[6];
        System.arraycopy(paramMsg, 8, lockMacBytes, 0, 6);
        param.setLockMac(BytesUtils.formatHexString(lockMacBytes));

        param.setCode(unlockResult);
        param.setLockNo(lockMacNoMap.get(gatewayMac));
        BackgroundService.pushToBackground(remoteUnLockPushUrl, param);
        logger.info("远程开锁结果推送：" + JSON.toJSONString(param));
    }

    public static void addRemoteUnLockTask(String gatewayMac, String remoteUnLockCommand) {
        remoteUnLockMap.put(gatewayMac, remoteUnLockCommand);
    }

    public static String getRemoteUnLockTask(String gatewayMac) {
        return remoteUnLockMap.remove(gatewayMac);
    }

    public static void main(String[] args) {
        byte[] bytes = new byte[32];
        String pwd = "01,02,03,04,05,06,07,08,09";
        //lockPwd
        String[] lockPwd = StringUtils.split(pwd, ",");
        int k = 27;
        for (int i = lockPwd.length - 1; i >= 0; i--) {
            bytes[k] = (byte) ScaleUtil.HexToInt(lockPwd[i]);
            k--;
        }
        while (k >= 16) {
            bytes[k] = 92;
            k--;
        }
        System.out.println(BytesUtils.formatHexString(bytes));
    }

    public static byte[] remoteUnlock(RemoteUnLockParam param) {
        byte[] bytes = new byte[32];
        bytes[0] = 13;
        bytes[1] = 10;
        //head
        bytes[2] = (byte) 204;
        //random code
        bytes[3] = 00;
        bytes[4] = 28;
        //xu lie code
        bytes[5] = (byte) xulieCode.incrementAndGet();
        //service code
        bytes[6] = 01;
        // commond code
        bytes[7] = (byte) ScaleUtil.HexToInt(param.getCommand());
        //lockNo
        String[] lockNo = StringUtils.split(param.getUnLockNo(), ",");
        bytes[8] = (byte) ScaleUtil.HexToInt(lockNo[0]);
        bytes[9] = (byte) ScaleUtil.HexToInt(lockNo[1]);
        //lockMac
        String[] lockMac = StringUtils.split(param.getLockMac(), ",");
        bytes[10] = (byte) ScaleUtil.HexToInt(lockMac[0]);
        bytes[11] = (byte) ScaleUtil.HexToInt(lockMac[1]);
        bytes[12] = (byte) ScaleUtil.HexToInt(lockMac[2]);
        bytes[13] = (byte) ScaleUtil.HexToInt(lockMac[3]);
        bytes[14] = (byte) ScaleUtil.HexToInt(lockMac[4]);
        bytes[15] = (byte) ScaleUtil.HexToInt(lockMac[5]);

        //lockPwd
        String[] lockPwd = StringUtils.split(param.getLockPwd(), ",");
        int k = 27;
        for (int i = lockPwd.length - 1; i >= 0; i--) {
            bytes[k] = (byte) ScaleUtil.HexToInt(lockPwd[i]);
            k--;
        }
        while (k >= 16) {
            bytes[k] = 92;
            k--;
        }

        //verify code
        bytes[28] = 01;
        bytes[29] = (byte) 170;
        bytes[30] = 13;
        bytes[31] = 10;
        addRemoteUnLockTask(param.getGatewayMac(), BytesUtils.formatHexString(bytes));
        lockMacNoMap.put(param.getGatewayMac(), param.getUnLockNo());
        return bytes;
    }
}
