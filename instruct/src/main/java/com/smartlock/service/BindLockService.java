package com.smartlock.service;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.smartlock.param.BindLockPushParam;
import com.smartlock.param.LockStatusParam;
import com.smartlock.util.BytesUtils;

public class BindLockService {

    private static final String bindLockUrl       = "http://116.62.142.20/MicroService/bindlock";
    private static final String bindLockStatusUrl = "http://116.62.142.20/MicroService/bindLockStatus";
    private static Logger       logger            = LoggerFactory.getLogger(BindLockService.class);

    private static void pushLockMsgToBackground(byte[] paramMsg, byte[] returnMsg) {
        BindLockPushParam param = new BindLockPushParam();
        byte[] netMac = new byte[6];
        System.arraycopy(paramMsg, 8, netMac, 0, 6);
        byte[] lockMac = new byte[6];
        System.arraycopy(paramMsg, 14, lockMac, 0, 6);
        byte[] lockNo = new byte[2];
        System.arraycopy(returnMsg, 7, lockNo, 0, 2);
        byte[] lockPwd = new byte[6];
        System.arraycopy(returnMsg, 9, lockPwd, 0, 6);

        param.setLockMac(BytesUtils.formatHexString(lockMac));
        param.setLockNo(BytesUtils.formatHexString(lockNo));
        param.setLockPwd(BytesUtils.formatHexString(lockPwd));
        param.setNetMac(BytesUtils.formatHexString(netMac));
        BackgroundService.pushToBackground(bindLockUrl, param);

        System.out.println("绑定锁推送，绑定锁入参：" + BytesUtils.formatHexString(paramMsg) + "绑定锁出参："
                           + BytesUtils.formatHexString(returnMsg) + ",后台推送参数：" + JSON.toJSONString(param));
    }

    public static byte[] bindLock(byte[] paramMsg, String gatewayMac) throws InterruptedException {
        byte[] returnMsg = new byte[19];
        returnMsg[0] = 13;
        returnMsg[1] = 10;
        returnMsg[2] = (byte) 170;
        returnMsg[3] = paramMsg[4];
        //valid length
        returnMsg[4] = 11;
        //serialNum
        returnMsg[5] = paramMsg[6];
        //serviceCode
        returnMsg[6] = 32;
        //orderNu
        returnMsg[7] = 00;
        returnMsg[8] = (byte) new Random().nextInt(255);
        //pwd
        returnMsg[9] = (byte) new Random().nextInt(10);
        returnMsg[10] = (byte) new Random().nextInt(10);
        returnMsg[11] = (byte) new Random().nextInt(10);
        returnMsg[12] = (byte) new Random().nextInt(10);
        returnMsg[13] = (byte) new Random().nextInt(10);
        returnMsg[14] = (byte) new Random().nextInt(10);
        //verify code
        returnMsg[15] = paramMsg[20];
        returnMsg[16] = (byte) 204;
        returnMsg[17] = 13;
        returnMsg[18] = 10;
        pushLockMsgToBackground(paramMsg, returnMsg);

        LockStatusParam statusParam = new LockStatusParam();
        byte[] lockNo = new byte[2];
        System.arraycopy(returnMsg, 7, lockNo, 0, 2);
        statusParam.setLockNo(BytesUtils.formatHexString(lockNo));

        byte[] lockMacBytes = new byte[6];
        System.arraycopy(paramMsg, 14, lockMacBytes, 0, 6);
        statusParam.setLockMac(BytesUtils.formatHexString(lockMacBytes));

        Thread.sleep(10 * 1000l);//线程暂停10s，请求后台判断app绑定是否成功
        JSONObject jsonObject = BackgroundService.reqBackground(bindLockStatusUrl, statusParam);
        if (jsonObject.getIntValue("success") == 0) {//app锁定失败
            logger.info("app绑定失败，网关mac=" + gatewayMac + ",绑定指令为：" + BytesUtils.formatHexString(paramMsg));
            return null;
        }
        return returnMsg;//app绑定成功
    }
}
