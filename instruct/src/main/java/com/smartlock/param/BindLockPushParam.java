package com.smartlock.param;

public class BindLockPushParam {

    private String netMac;

    private String lockMac;

    private String lockNo;

    private String lockPwd;

    public String getNetMac() {
        return netMac;
    }

    public void setNetMac(String netMac) {
        this.netMac = netMac;
    }

    public String getLockMac() {
        return lockMac;
    }

    public void setLockMac(String lockMac) {
        this.lockMac = lockMac;
    }

    public String getLockNo() {
        return lockNo;
    }

    public void setLockNo(String lockNo) {
        this.lockNo = lockNo;
    }

    public String getLockPwd() {
        return lockPwd;
    }

    public void setLockPwd(String lockPwd) {
        this.lockPwd = lockPwd;
    }
}
