package com.smartlock.param;

public class ApiResult<T> {

    private String reCode;

    private String message;

    private T      reData;

    public static <T> ApiResult<T> ok(T t, String message) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setMessage(message);
        apiResult.setReCode("00");
        apiResult.setReData(t);
        return apiResult;
    }

    public static <T> ApiResult<T> fail(T t, String message) {
        ApiResult<T> apiResult = new ApiResult();
        apiResult.setMessage(message);
        apiResult.setReCode("01");
        apiResult.setReData(t);
        return apiResult;
    }

    public String getReCode() {
        return reCode;
    }

    public void setReCode(String reCode) {
        this.reCode = reCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getReData() {
        return reData;
    }

    public void setReData(T reData) {
        this.reData = reData;
    }

}
