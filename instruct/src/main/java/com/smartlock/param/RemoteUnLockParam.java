package com.smartlock.param;

public class RemoteUnLockParam {

    private String command = "01";

    private String unLockNo;

    private String lockMac;

    private String gatewayMac;

    private String lockPwd;

    public String getGatewayMac() {
        return gatewayMac;
    }

    public void setGatewayMac(String gatewayMac) {
        this.gatewayMac = gatewayMac;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUnLockNo() {
        return unLockNo;
    }

    public void setUnLockNo(String unLockNo) {
        this.unLockNo = unLockNo;
    }

    public String getLockMac() {
        return lockMac;
    }

    public void setLockMac(String lockMac) {
        this.lockMac = lockMac;
    }

    public String getLockPwd() {
        return lockPwd;
    }

    public void setLockPwd(String lockPwd) {
        this.lockPwd = lockPwd;
    }
}
