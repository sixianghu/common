package sort;

public class InsertSort extends Sort {

    public static void main(String[] args) {
        sort = new InsertSort();
        sort.run();
    }

    @Override
    protected void sort(int[] nums, int start, int end) {
        for (int i = 1; i < nums.length; i++) {
            int temp = nums[i];
            int targetIndex = i;
            while (targetIndex > 0 && temp < nums[targetIndex - 1]) {
                nums[targetIndex] = nums[targetIndex - 1];
                targetIndex--;
            }
            nums[targetIndex] = temp;
        }
    }
}
