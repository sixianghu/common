package sort;

public class BubbleSort extends Sort {

    public static void main(String[] args) {
        sort = new BubbleSort();
        sort.run();
    }

    @Override
    protected void sort(int[] nums, int start, int end) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i; j < nums.length; j++) {
                if (nums[j] < nums[i]) {
                    swap(nums, i, j);
                }
            }
        }
    }
}
