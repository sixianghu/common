package sort;

public class SelectSort extends Sort {

    public static void main(String[] args) {
        sort = new SelectSort();
        sort.run();
    }

    @Override
    protected void sort(int[] nums, int start, int end) {
        for (int i = 0; i < nums.length; i++) {
            int minNum = nums[i];
            int targetIndex = i;
            for (int j = i; j < nums.length; j++) {
                if (nums[j] < minNum) {
                    minNum = nums[j];
                    targetIndex = j;
                }
            }
            swap(nums, i, targetIndex);
        }
    }
}
