package sort;

public abstract class Sort {

    public static final int[] nums = { 8, 5, 1, 6, 3, 9, 0, 2, 3 };

    protected static Sort     sort;

    public static void swap(int[] nums, int left, int right) {
        int temp = nums[right];
        nums[right] = nums[left];
        nums[left] = temp;
    }

    public static void print(int[] nums) {
        for (int num : nums) {
            System.out.printf(num + ",");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        print(nums);
    }

    public void run() {
        sort.sort(nums, 0, nums.length - 1);
        print(nums);
    }

    protected abstract void sort(int[] nums, int start, int end);

}
