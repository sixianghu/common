package structure;

import sort.Sort;

/**
 * 栈
 */
public class Stack {

    static int[] stack = new int[10];
    static int   index = 0;

    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            push(i);
            //            Sort.print(stack);
        }
        for (int i = 0; i < 15; i++) {
            Sort.print(stack);
            pop();
            Sort.print(stack);
            System.out.println("-----------");
        }
    }

    public static void push(int num) {
        if (index == stack.length) {
            throw new IllegalArgumentException("栈中已存满，不能往栈中压入数据了！");
        }
        stack[index] = num;
        index++;
    }

    public static int pop() {
        if (index == 0) {
            throw new IllegalArgumentException("栈中没有数据，不能弹出数据了！");
        }
        index--;
        int num = stack[index];
        stack[index] = 0;
        return num;
    }
}
