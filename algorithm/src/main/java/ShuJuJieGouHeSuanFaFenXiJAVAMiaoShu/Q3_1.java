package ShuJuJieGouHeSuanFaFenXiJAVAMiaoShu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Q3_1 {

  private static ArrayList<Integer> L;
  private static LinkedList<Integer> P;

  public static void main(String[] args) {
    L = new ArrayList<>();
    L.add(4);
    L.add(3);
    L.add(8);
    L.add(9);
    L.add(6);
    L.add(1);

    P = new LinkedList<>();
    P.add(1);
    P.add(3);
    P.add(4);
    P.add(6);
    printLots(L, P);

  }

  public static void printLots(List<Integer> L, LinkedList<Integer> P) {
    Iterator<Integer> pIter = P.iterator();
    while (pIter.hasNext()) {
      int pItem = pIter.next();
      if (L.size() > (pItem - 1)) {
        System.out.println(L.get(pItem - 1));
      }
    }
  }

}
