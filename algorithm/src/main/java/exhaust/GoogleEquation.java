package exhaust;

import java.util.ArrayList;
import java.util.List;

/**
 * WWWDOT-GOOGLE=DOTCOM
 * Created by donghongli on 2017-03-17.
 */
public class GoogleEquation {
    public static void main(String[] args) {
        boolean[] nums = { false, false, false, false, false, false, false, false, false, false };
        List<CharItem> charItems = new ArrayList<>();
        charItems.add(new CharItem('W', -1, Boolean.TRUE));
        charItems.add(new CharItem('D', -1, Boolean.TRUE));
        charItems.add(new CharItem('O', -1, Boolean.FALSE));
        charItems.add(new CharItem('T', -1, Boolean.FALSE));
        charItems.add(new CharItem('G', -1, Boolean.TRUE));
        charItems.add(new CharItem('L', -1, Boolean.FALSE));
        charItems.add(new CharItem('E', -1, Boolean.FALSE));
        charItems.add(new CharItem('C', -1, Boolean.FALSE));
        charItems.add(new CharItem('M', -1, Boolean.FALSE));
        searchResult(charItems, nums, 1);
    }

    public static void searchResult(List<CharItem> charItems, boolean[] nums, int charSize) {
        if (charSize > charItems.size()) {
            checkResult(charItems);
            return;
        }
        CharItem charItem = charItems.get(charSize - 1);
        for (int i = 0; i < nums.length; i++) {
            if (charItem.isNumFirstChar && i == 0) {
                continue;
            }
            if (nums[i] == Boolean.FALSE) {
                charItem.value = i;
                nums[i] = true;
                searchResult(charItems, nums, charSize + 1);
                nums[i] = false;
            }
        }
    }

    /**
     * 检查查询结果是否正确
     *
     * @param charItems
     */
    private static void checkResult(List<CharItem> charItems) {
        int wValue = getValueByCode(charItems, 'W');
        int dValue = getValueByCode(charItems, 'D');
        int oValue = getValueByCode(charItems, 'O');
        int tValue = getValueByCode(charItems, 'T');
        int gValue = getValueByCode(charItems, 'G');
        int lValue = getValueByCode(charItems, 'L');
        int eValue = getValueByCode(charItems, 'E');
        int cValue = getValueByCode(charItems, 'C');
        int mValue = getValueByCode(charItems, 'M');
        int wwwdot = wValue * 100000 + wValue * 10000 + wValue * 1000 + dValue * 100 + oValue * 10 + tValue;
        int google = gValue * 100000 + oValue * 10000 + oValue * 1000 + gValue * 100 + lValue * 10 + eValue;
        int dotcom = dValue * 100000 + oValue * 10000 + tValue * 1000 + cValue * 100 + oValue * 10 + mValue;
        if (wwwdot - google == dotcom) {
            printCharValues(charItems, wwwdot, google, dotcom);
        }
    }

    /**
     * 获取字符对应的值
     *
     * @param charItems
     * @param code
     * @return
     */
    private static int getValueByCode(List<CharItem> charItems, char code) {
        for (CharItem charItem : charItems) {
            if (charItem.code == code) {
                return charItem.value;
            }
        }
        return 0;
    }

    /**
     * 打印出字符对应的值。
     *
     * @param charItems
     * @param wwwdot
     * @param google
     * @param dotcom
     */
    private static void printCharValues(List<CharItem> charItems, int wwwdot, int google, int dotcom) {
        for (CharItem charItem : charItems) {
            System.out.print(charItem.code + "=" + charItem.value + ",");
        }
        System.out.println();
        System.out.println(wwwdot + "-" + google + "=" + dotcom);
    }

    /**
     * 字符对应的数据结构
     */
    static class CharItem {
        public char    code;
        public int     value;
        public boolean isNumFirstChar;

        public CharItem(char code, int value, boolean isNumFirstChar) {
            this.code = code;
            this.isNumFirstChar = isNumFirstChar;
            this.value = value;
        }
    }

}